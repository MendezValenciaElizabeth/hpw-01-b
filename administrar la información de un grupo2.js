/*creacion de la variable grupo */
var Grupo = function(
_clave,
_nombre,
_docente,
_materias,
_alumnos
    ){
        if(!_clave) {
            return {
                "exito" : false,
                "mensaje" : "La clave del grupo no ha sido definida"
            };
        }
        return {
            "clave": _clave,
            "nombre" : _nombre,
            "docente" : _docente,
            "materias" : _materias,
            "alumnos" : _alumnos
        };
        };
 /*creacion de la variable docente*/        
var Docente = function (
    _clave,
    _nombre,
    _apellidos,
    _grado_academico
    ){
        if(!_clave){
            return {
                "exito": false,
                "mensaje" : "la clave del docente no ha sido definida"
            };
        }
        return {
            "clave" : _clave,
            "nombre" : _nombre,
            "apellidos" : _apellidos,
            "grado_academico" : _grado_academico
        };
    };
    
/*creación de la variable materia*/
var Materia = function (_clave, _nombre)
{ return {
    "clave" : _clave,
    "nombre" : _nombre
}
};

/*creacion de la variable alumno*/
var Alumno = function (_clave, _nombre, _apellidos, _calificaciones)
{
    return {
        "clave" : _clave,
        "nombre" : _nombre,
        "apellidos" : _apellidos,
        "calificaciones" : _calificaciones
    }
};
/***************************************funciones***********************************/
/**************GRUPO*****************/
/*Crear un grupo*/
var grupo1 = Grupo ('G10','A'); // crear un grupo
var grupo2 = Grupo ('G60','D'); // crear otro grupo

/*función para saber si existe un grupo*/
function existeGrupo(g, c){
    if(g.clave ===c)
    {
        return {
            "exito" : true,
            "mensaje" : "El grupo: " + g.nombre + " si existe"
        };
}
}

/*función para eliminar un grupo */
function eliminaGrupo (g) {
    if(!g.clave)  {
        return         {
            "exito" : false,
            "mensaje" : "El grupo: " + g.nombre + " no existe"
        };
    }
    g.clave = undefined;
    return {
        "exito" : true,
       "mensaje" :  "El grupo: " + g.nombre + " ha sido eliminado"
    };
}
/**************DOCENTE**************/
/*crear  docentes*/
var docente1 = Docente('E12','Elizabeth','Méndez Valencia');
var docente2 = Docente (undefined, 'Elizabeth', 'Méndez Valencia');
var docente3 = Docente('E12','Elizabeth','Méndez Valencia', 'Ing. Sistemas Comp.');

/*función para asignar un docente al grupo*/
function asignarDocenteGrupo(g, d) {
    if(!g.docente) {
    g.docente = d;
    return {
        "exito" :true,
        "mensaje" : "El docente ha sido asignado al grupo " + g.nombre
    };
    }
    return {
        "exito" :false,
        "mensaje" : "El grupo: " + g.nombre + " ya cuenta con docente"
};
}

/*función para saber si existe o no un docente en el grupo*/
function existeDocenteGrupo(g, cd) {
    if(g.docente.clave === cd) {
        return {
            "exito" : true,
            "mensaje" : "El docente" + 
            g.docente.nombre + "" + 
            g.docente.apellidos + " está asignado al grupo" + 
            g.nombre
        };
    }
    return {
        "exito" : false, 
        "mensaje" : "el docente no está asignado al grupo" + g.nombre
    };
}

/*función para borrar al docente de un grupo*/
function borrarDocenteGrupo (g, d) {
    if(!g.docente) {
        return {
            "exito" : false,
            "mensaje" : "El grupo " + g.nombre + " no tiene docente asignado"
        };
    }
    // delete.g.docente;
    g.docente = undefined;
    return {
        "exito" : true,
        "mensaje" : "El docente ha sido borrado del grupo " + g.nombre
    };
}

/************ALUMNOS*************/
var alumno1 = Alumno('10161718', 'Juan', 'Perez'); //crear alumno

/*funcion para agregar un alumno en un grupo*/
function agregarAlumnoGrupo (g, a)
{
    if(!g.alumno)
    {
        g.alumno = [];
    }
    if(!existeAlumnoGrupo(g, a.clave))
    {
        g.alumno.push(a);
        return true;
    }
    return false;
}

/*funcion para saber si existe un alumno en el grupo*/
function existeAlumnoGrupo (g, ca)
{
    if(!g.alumno && g.alumno.length ===0)
    {
        return false;
    }
    for (var i=0; i<g.alumno.length; i++)
    {
        if(g.alumno [i].clave === ca)
        {
            return true;
        }
    }
    return false;
}

/*funcion para eliminar un alumno del grupo*/
function borrarAlumnoGrupo (g, a) {
    if(!g.alumno) {
        return {
            "exito" : false,
            "mensaje" : "El grupo: " + g.nombre + " no tiene al alumno asignado"
        };
    }
    g.alumno = undefined;
    return {
        "exito" : true,
        "mensaje" : "El alumno ha sido eliminado del grupo: " + g.nombre
    };
}

/************MATERIAS***************/
var materia1 = Materia('M03', 'Fisica');
var materia2 = Materia('M05', 'Historia');

/*funcion para agregar una materia al grupo*/
function agregarMateriaGrupo (g,m)
{
    if(!g.materia)
    {
        g.materia = [];
    }
    if(!existeMateriaGrupo(g, m.clave))
    {
        g.materia.push(m);
        return true;
    }
    return false;
}

/*funcion para saber si existe una materia en el grupo*/
function existeMateriaGrupo (g, cm)
{
    if(!g.materia && g.materia.length ===0)
    {
        return false;
    }
    for (var i= 0; i<g.materia.length; i++)
    {
        if(g.materia [i].clave ===cm)
        {
            return true;
        }
    }
        return false;
}

/*funcion para borrar una materia del grupo*/
function borrarMateriaGrupo (g, m) {
    if(!g.materia) {
        return {
            "exito" : false,
            "mensaje" : "El grupo " + g.nombre + " no tiene esta materia asignada"
        };
    }
    g.materia = undefined;
    return {
        "exito" : true,
        "mensaje" : "La materia ha sido borrada del grupo " + g.nombre
    };
}
/**************************PRUEBAS********************/
existeGrupo(grupo1, 'G10'); //existe el grupo
existeGrupo(grupo2, 'G60'); //existe el grupo
eliminaGrupo(grupo1); //elimina grupo
asignarDocenteGrupo(grupo1, docente1); // asigna docente al grupo
asignarDocenteGrupo(grupo2, docente2); //asigna docente al grupo
existeDocenteGrupo (grupo1, 'D10'); //existe docente en el grupo
existeDocenteGrupo (grupo2, 'D10'); //existe docente en el grupo
borrarDocenteGrupo(grupo1); //borra docente del grupo
agregarAlumnoGrupo(grupo1, alumno1); //agrega alumno al grupo
existeAlumnoGrupo(grupo1, '10161718'); //existe alumno en grupo
borrarAlumnoGrupo(grupo1, alumno1); //borra alumno del grupo
agregarMateriaGrupo(grupo1, materia1); //agrega materia al grupo
agregarMateriaGrupo(grupo1, materia2); //agrega otra materia al mismo grupo
existeMateriaGrupo(grupo1, 'M03'); //existe materia en el grupo
existeMateriaGrupo(grupo1, 'M05'); //existe materia en el grupo
borrarMateriaGrupo(grupo1, materia1); //borra materia del grupo